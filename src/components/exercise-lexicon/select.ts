import { LitElement, css, html } from 'lit'
import { customElement, property} from 'lit/decorators.js'

import { TEntity } from '../../types/entity'
import { TExerciceLexicon } from '../../types/exercise-lexicon'

@customElement('select-exercise-lexicon')
export class SelectExerciseLexicon extends LitElement {

  @property({ type: Object})
  items: TEntity<TExerciceLexicon>[] = []

  @property({ type: String})
  selected=""

  handleSelect = (ev: Event) => {

    const selected = (ev.target as HTMLSelectElement) .value
    const event = new CustomEvent("select", { detail: selected });
    this.dispatchEvent(event)
    this.selected = selected
  }

  render(){

    const options = []
if (this.items){
    for(let i = 0; i < this.items.length ; i++ ){
      if(this.items[i].meta.uuid === this.selected){
        options.push(
          html`
            <option selected value="${this.items[i].meta.uuid}">${this.items[i].title}</option> 
          `
        )
      }else{
        options.push(
          html`
            <option value="${this.items[i].meta.uuid}">${this.items[i].title}</option> 
          `
        )
      }
    }
}
    return html`
    <label for="selection">Choisir un exercice</label>
    <select id="selection" @input=${this.handleSelect}>
     ${options}
    <select>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'select-exercise-lexicon': SelectExerciseLexicon
  }
}