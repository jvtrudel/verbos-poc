import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'



type TEsVerb = {
  terminacion: "er" | "ar" | "ir"
  raiz: string
  infinitivo: string
}

type TEsVerbState ={
  time: "presente" | "preterito" | "futur"
  number: 0 | 1 // singular | plural
  person: 0 | 1 | 2 
  gender?: 0 | 1 | 2
}

type TEsVerbStyle = {
  raiz?: string
  terminacion?: string
  verbo?: string
  pronom?: string
}

const verbForms ={

  "presente" : {
  "ar": [ ["o", "as", "a"], ["amos", "aís", "an"] ],
  "ir": [ ["o", "es", "e"], ["imos", "is", "en"] ],
  "er": [ ["o", "es", "e"], ["emos", "éis", "en"] ],
},
"preterito": {
  "ar": [ ["é", "aste", "ó"], ["amos", "asteis", "aron"] ],
  "ir": [ ["í", "iste", "ó"], ["imos", "isteis", "ieron"] ],
  "er": [ ["í", "iste", "ó"], ["imos", "isteis", "ieron"] ],
},
"futur": {
  "ar": [ ["é", "ás", "á"], ["emos", "éis", "án"] ],
  "ir": [ ["é", "ás", "á"], ["emos", "éis", "án"] ],
  "er": [ ["é", "ás", "á"], ["emos", "éis", "án"] ],
}
}

const pronoun = [ ["yo", "tu", ["el", "ella", "usted"] ], ["nosotros", "vosotros", ["ellos", "ellas", "ustedes"]] ]

const genStyledVerb = (verb: TEsVerb, vs: TEsVerbState, style: TEsVerbStyle ) => {

  if (vs.gender !== undefined){
    return html`<p class="${style.verbo}"><span class="${style.pronom}">${pronoun[vs.number][vs.person][vs.gender]}</span> <span class="${style.raiz}">${verb.raiz}</span><span class="${style.terminacion}">${verbForms[vs.time][verb.terminacion][vs.number][vs.person]}</span></p>`
  }else{
    return html`<p class="${style.verbo}"><span class="${style.pronom}">${pronoun[vs.number][vs.person]}</span> <span class="${style.raiz}">${verb.raiz}</span><span class="${style.terminacion}">${verbForms[vs.time][verb.terminacion][vs.number][vs.person]}</span></p>`
  }

}

const genVerb = (verb: TEsVerb, vs: TEsVerbState ) => {

  if (vs.gender !== undefined){
    return html`<p>${pronoun[vs.number][vs.person][vs.gender]} ${verb.raiz}${verbForms[vs.time][verb.terminacion][vs.number][vs.person]}</p>`
  }else{
    return html`<p>${pronoun[vs.number][vs.person]} ${verb.raiz}${verbForms[vs.time][verb.terminacion][vs.number][vs.person]}</p>`
  }

}

const genTableMxGender = (verb: TEsVerb, gender: 0 | 1 | 2) => {

  return html` 
        <div class="verb-table-mxgender">
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:1 })} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:2, gender: gender})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:2, gender: 2})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:2, gender})} </div>
        </div>
  `
}


const genTableAll = (verb: TEsVerb) => {

  return html` 
        <div class="verb-table-all">
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:1 })} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:2, gender: 0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:2, gender: 1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:2, gender: 2})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:2, gender: 0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:2, gender: 1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:2, gender: 2})} </div>
        </div>
  `
}
const genTableAll3times = (verb: TEsVerb) => {

  return html` 
        <div class="verb-table-all-3times">
           <div class="verb-table-header"> preterito </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "preterito", number: 0, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "preterito", number: 0, person:1 })} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "preterito", number: 0, person:2, gender: 0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "preterito", number: 0, person:2, gender: 1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "preterito", number: 0, person:2, gender: 2})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "preterito", number: 1, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "preterito", number: 1, person:1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "preterito", number: 1, person:2, gender: 0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "preterito", number: 1, person:2, gender: 1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "preterito", number: 1, person:2, gender: 2})} </div>

           <div class="verb-table-header"> presente </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:1 })} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:2, gender: 0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:2, gender: 1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:2, gender: 2})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:2, gender: 0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:2, gender: 1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:2, gender: 2})} </div>

           <div class="verb-table-header"> futur </div>
            <div class="verb-table-item"> ${genVerb(verb, {time: "futur", number: 0, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "futur", number: 0, person:1 })} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "futur", number: 0, person:2, gender: 0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "futur", number: 0, person:2, gender: 1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "futur", number: 0, person:2, gender: 2})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "futur", number: 1, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "futur", number: 1, person:1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "futur", number: 1, person:2, gender: 0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "futur", number: 1, person:2, gender: 1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "futur", number: 1, person:2, gender: 2})} </div>
        </div>
  `
}
const genTableAllMx = (verb: TEsVerb) => {

  return html` 
        <div class="verb-table-all">
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:1 })} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:2, gender: 0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:2, gender: 1})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 0, person:2, gender: 2})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:2, gender: 2})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:2, gender: 0})} </div>
           <div class="verb-table-item"> ${genVerb(verb, {time: "presente", number: 1, person:2, gender: 1})} </div>
        </div>
  `
}

/**
 * Dénombrement et classification des variaitons possibles
 *   - majuscule
 *   - couleur: pronom, racine, terminaison
 *   - clicable: pronom, racine, terminaison
 */



@customElement('es-regular-verb')
export class EsRegularVerb extends LitElement {


  @property({type: Object })
  verb: TEsVerb | undefined 


 


  render() {

    let uiTable
    if( this.verb !== undefined){
       
      uiTable=html`
        <h1>${this.verb.infinitivo}</h3>
        <div class="hflex flex-center">
           <div>
           <h4> Mx Gender M </h4>
             ${genTableMxGender(this.verb, 0)}
           </div>
           <div>
           <h4> Mx Gender F </h4>
             ${genTableMxGender(this.verb, 1)}
           </div>
           <div>
           <h4> Mx Gender N </h4>
             ${genTableMxGender(this.verb, 2)}
           </div>
        </div>

        <div class="hflex flex-center">
           <div>
        <h4> All </h4>
        ${genTableAll(this.verb)}
           </div>
           <div>
        <h4> All Mx</h4>
        ${genTableAllMx(this.verb)}
           </div>
           <div>
           ${genStyledVerb(this.verb, {time: "presente", number: 0, person: 1}, {pronom: "big-blue"})}
           ${genStyledVerb(this.verb, {time: "futur",number: 0, person: 0}, {raiz: "blue", terminacion: "red" })}
           ${genStyledVerb(this.verb, {time: "preterito", number: 1, person: 2, gender: 1}, {verbo: "bg-yellow", terminacion: "red" })}
           
           </div>
        </div>

        <div class="hflex flex-center">
           <div>
        <h4> 3 tiempos </h4>
        ${genTableAll3times(this.verb)}
           </div>
           </div>
      `
    }


    return html`
      <div class="vflex flex-center">

       

      ${uiTable}
      </div>
    `
  }


  static styles = css`
    :host {
      max-width: 100%;
      margin: 0 auto;
      padding: 2rem;
      text-align: center;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .flex-hleft-vcenter {
      display: flex;
      justify-content: start;
      align-items: center;
    }

    .flex-hcenter {
      display: flex;
    }

    .hflex {
      display: flex;
      flex-direction: row;
      gap: 3rem;
    }

    .vflex {
      display: flex;
      flex-direction: column;
    }

    .flex-center {
      justify-content: center;
      align-items: center
    }


    .verb-table-mxgender{
      max-width: 400px;
      display: grid;
      grid-template-columns: 1fr 1fr ; 
      grid-template-rows: 1fr 1fr 1fr;
      grid-auto-flow: column;

      border: 1px solid black;
    }

    .verb-table-all {
      max-width: 400px;
      display: grid;
      grid-template-columns: 1fr 1fr ; 
      grid-template-rows: 1fr 1fr 1fr 1fr 1fr;
      grid-auto-flow: column;

      border: 1px solid black;
    }
    
    .verb-table-all-3times {
      max-width: 400px;
      display: grid;
      grid-template-columns: 1fr 1fr ; 
      grid-template-rows: 1fr  1fr 1fr 1fr 1fr 1fr   1fr 1fr 1fr 1fr 1fr;
      grid-auto-flow: column;

      border: 1px solid black;
    }


    .verb-table-item {

      padding: 0 1.5rem;
      border: 1px solid black;
    }
    .verb-table-header{
      text-transform: uppercase;
      padding: 0 1.5rem;
      padding-top: 2rem;
      font-weight: 900;

      border: 1px solid black;
    }

    .big-blue { color: blue; font-weight: 900;}
    .blue { color: blue; }
    .green { color: green; }
    .red { color: red; }

    .bg-yellow { background-color: yellow; }

    .dev {
      border: 1px dashed red;
    }

  `
}

declare global {
  interface HTMLElementTagNameMap {
    'es-regular-verb': EsRegularVerb
  }
}
