import { LitElement, css, html } from 'lit'
import { customElement, property} from 'lit/decorators.js'

import { TEntity } from '../../types/entity'
import { TText } from '../../types/text'

@customElement('select-sentence')
export class SelectSentence extends LitElement {

  @property({ type: Object})
  items: TEntity<TText>[] = []

  @property({ type: String})
  selected=""

  handleSelect = (ev: Event) => {

    const selected = (ev.target as HTMLSelectElement) .value
    const event = new CustomEvent("select", { detail: selected });
    this.dispatchEvent(event)
    this.selected = selected
  }

  render(){

    const options = []
if (this.items){
    for(let i = 0; i < this.items.length ; i++ ){
      if(this.items[i].meta.uuid === this.selected){
        options.push(
          html`
            <option selected value="${this.items[i].meta.uuid}">${this.items[i].str}</option> 
          `
        )
      }else{
        options.push(
          html`
            <option value="${this.items[i].meta.uuid}">${this.items[i].str}</option> 
          `
        )
      }
    }
}
    return html`
    <label for="selection">Choisir une phrase</label>
    <select id="selection" @input=${this.handleSelect}>
     ${options}
    <select>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'select-sentence': SelectSentence
  }
}