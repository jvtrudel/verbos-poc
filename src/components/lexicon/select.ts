import { LitElement, css, html } from 'lit'
import { customElement, property} from 'lit/decorators.js'

import { TEntity } from '../../types/entity'
import { TLexicon } from '../../types/lexicon'

@customElement('select-lexicon')
export class SelectLexicon extends LitElement {

  @property({ type: Object})
  lexicons: TEntity<TLexicon>[] = []

  @property({ type: String})
  selected=""

  handleSelect = (ev: Event) => {

    const selected = (ev.target as HTMLSelectElement) .value
    const event = new CustomEvent("select", { detail: selected });
    this.dispatchEvent(event)
    this.selected = selected
  }

  render(){

    const options = []
if (this.lexicons){
    for(let i = 0; i < this.lexicons.length ; i++ ){
      if(this.lexicons[i].meta.uuid === this.selected){
        options.push(
          html`
            <option selected value="${this.lexicons[i].meta.uuid}">${this.lexicons[i].title}</option> 
          `
        )
      }else{
        options.push(
          html`
            <option value="${this.lexicons[i].meta.uuid}">${this.lexicons[i].title}</option> 
          `
        )
      }
    }
}
    return html`
    <label for="selection">Choisir un lexique</label>
    <select id="selection" @input=${this.handleSelect}>
     ${options}
    <select>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'select-lexicon': SelectLexicon
  }
}