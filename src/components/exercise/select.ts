import { LitElement, css, html } from 'lit'
import { customElement, property} from 'lit/decorators.js'

import { TEntity } from '../../types/entity'
import { TExerciseForm } from '../../types/exercise'

@customElement('select-exercise')
export class SelectExercise extends LitElement {

  @property({ type: Object})
  exercises: TEntity<TExerciseForm>[] = []


  handleSelect = (ev: Event) => {

    const selected = (ev.target as HTMLSelectElement) .value
    const event = new CustomEvent("select", { detail: selected });
    this.dispatchEvent(event)
  }

  render(){

    const options = []
if (this.exercises){
    for(let i = 0; i < this.exercises.length ; i++ ){
        options.push(
            html`
            <option value="${this.exercises[i].meta.uuid}">${this.exercises[i].header.title}</option> 
            `
        )
    }
}
    return html`
    <label for="selection">Choisir un exercice</label>
    <select id="selection" @input=${this.handleSelect}>
     ${options}
    <select>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'select-exercise': SelectExercise
  }
}