import { LitElement, css, html } from 'lit'
import { customElement, property, query } from 'lit/decorators.js'

import { v4 as uuidv4 } from 'uuid';

import { TEntity } from '../../types/entity';
import { TExerciseForm, TExerciseHeader, TExerciseContent, TExerciseHints } from '../../types/exercise'

@customElement('new-exercise')
export class NewExercise extends LitElement {

  @property({ type: Object})
  header: TExerciseHeader | null = null

  @property({ type: Object})
  content: TExerciseContent | null = null
  
  @property({ type: Object})
  hints: TExerciseHints | null = null

  @query("#title")
  titleQ: any

  @query("#description")
  descriptionQ: any

  @query("#goal")
  goalQ: any
  
  @query("#category")
  categoryQ: any
  
  @query("#statement")
  statementQ: any

  @query("#verb")
  verbQ: any

  @query("#tense")
  tenseQ: any

  @query("#tense_hint")
  tenseHintQ: any

  @query("#success_hint")
  successHintQ: any

  @query("#verb_hint")
  verbHintQ: any
  
  
  handleSubmit(){

    const exercise: TEntity<TExerciseForm> = {
      meta: {
        uuid: uuidv4()
      }, 
      header: {
        title: (this.titleQ.value as string).trim() ,
        description: (this.descriptionQ.value as string),
        goal: (this.goalQ.value as string),
        category: (this.categoryQ.value as string),
      },
      content: {
        statement: (this.statementQ.value as string),
        verb: (this.verbQ.value as string),
        tense: (this.tenseQ.value as string),
      },
      hints: {
        success: (this.successHintQ.checked as Boolean),
        verb: (this.verbHintQ.checked as Boolean),
        tense: (this.tenseHintQ.checked as Boolean),
      }

    }

    console.log(exercise)
    const event = new CustomEvent("submit", { detail: exercise });
    this.dispatchEvent(event)

  }


  render(){

   

    const headerUI = html`
    <fieldset>
      <legend>Summary</legend>

      <label for="title">Title</label>
      <input type="text" id="title" value=${this.header && this.header.title}/>

      <label for="description">Description</label>
      <textarea id="description" value=${this.header && this.header.description}></textarea>

      <label for="goal">Educational Goal</label>
      <textarea id="goal" value=${this.header && this.header.goal}></textarea>

      <label for="category">Category</label>
      <input type="text" id="category" value=${this.header && this.header.category}/>

    </fieldset>
    `

    const contentUI = html`
    <fieldset>
      <legend>Exercise</legend>


      <label for="statement">Problem Statement</label>
      <textarea id="statement" value=${this.content && this.content.statement}></textarea>

      <label for="tense">Verbe tense</label>
      <input type="text" id="tense" value=${this.content && this.content.tense}/>

      <label for="verb">Verb</label>
      <input type="text" id="verb" value=${this.content && this.content.tense}/>

    </fieldset>
    `

    const cluesUI = html`
    <fieldset>
      <legend>Hints</legend>

      <input type="checkbox" id="verb_hint" value=${this.hints && this.hints.verb}/>
      <label for="verb_hint" class="checks">Verb</label>

      <input type="checkbox" id="tense_hint" value=${this.hints && this.hints.tense}/>
      <label for="tense_hint" class="checks">Tense</label>

      <input type="checkbox" id="success_hint" value=${this.hints && this.hints.success}/>
      <label for="success_hint" class="checks">Success</label>

    </fieldset>
    `

    return html`
    <div class="container">
    <form @submit=${this.handleSubmit } name="new-exercise-form">
      ${headerUI}
      ${contentUI}
      ${cluesUI}

      <input type="submit" id="submit" value="submit">
    </form>
    </div>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
      * {
        margin: 0;
        padding: 0;
      }
      .container {
        max-width: 800px;
      }
      fieldset {
        margin: 2em 0;
        padding: 1em 2em;
        border: solid 1px #ccc;
        border-radius: 6px;
        min-width: 200px;
      }
      legend {
        fon-size: 1.25em;
        padding: 0 .5em;
        color: #999;
      }
      /* Labels */
      label {
        display: block;
        margin-top: 1em;
      }
      .checks label {
        margin-top: 0;
      }
      label:first-of-type {
        margin-top: 0;
      }

      /* Inputs and textarea */
      input {
        padding: .5em;
        border: solid 1px #999;
      }

      input[type="text"] {
        width: 25em;
      }
      textarea {
        min-height: 8em;
        min-width: 90%;
        padding: .5em;
        border: solid 1px #999;
      }

      /* checkboxes */


      input[type="checkbox"] + label {
        display: inline-block;
        padding-top: 0;
        margin-top: 0;
      }
      input[type="checkbox"] { 
        margin-left: 1.5em;
      }
      input[type="checkbox"]:first-of-type { 
        margin-left: 0em;
      }

      /* button */
      input[type="submit"]{
        padding: .5em 1em;
        border-radius: 6px;
        background-color: #333;
        color: #fff;
        font-size: .8em;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'new-exercise': NewExercise
  }
}