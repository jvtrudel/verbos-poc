import { LitElement, css, html } from 'lit'
import { customElement, property, query, state} from 'lit/decorators.js'

import { TLexicon } from '../types/lexicon'
import { stringToSentence } from '../types/text'

@customElement('input-sentence')
export class InputSentence extends LitElement {

    @property({type: String})
    value= ""

    @property({type: Object})
    lexicon: TLexicon| undefined

    @query("#input")
    inputQ: HTMLInputElement | undefined

    @state()
    word: string | undefined

    handleInput = () => {

     if(this.lexicon && this.inputQ){

      this.value = this.inputQ.value

      const text = stringToSentence(this.inputQ.value)

      this.word = undefined
      for(let i=0; i < this.lexicon.words.length ; i++){
         if( text.graphies.includes(this.lexicon.words[i].toLocaleLowerCase())){
            this.word = this.lexicon.words[i]
         }
      }
     }

      const event = new CustomEvent("input", { detail: this.value });
      this.dispatchEvent(event)

    }

    render(){

      let msgUI 
      
      if( this.lexicon){
        if(this.word){
          msgUI = html`<small class="success">Contient le mot "${this.word}" </small>`
        }else{
          msgUI = html`<small class="failure">Ne contient aucun des mots du lexique" </small>`
        }
      }
        return html`
          <input id="input" @input=${this.handleInput} .value=${this.value} />
          </br>
          ${msgUI}

        `
    }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }

      .success {
        color: green;
      }
      .failure {
        color: red;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'input-sentence': InputSentence
  }
}
