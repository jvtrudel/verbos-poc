import { LitElement, PropertyValueMap, css, html } from 'lit'
import { customElement, property, state} from 'lit/decorators.js'

import { TLexicon } from '../types/lexicon'
import { TText} from '../types/text'
//import { TExerciceLexicon } from '../types/exercise-lexicon'


/*const punctuations: string[] = [".",",",":",";","'","\"","!","?","¡","¿"]
const removePunctuation = (word: string, punctuation: string[]): string => { 
      let _word = word
      for (let i=0; i < punctuation.length; i++){
        _word = _word.split(punctuation[i]).filter(e => e !== "" ).join()
      }
      return _word
    }
    const isCapitalized = (word: string): Boolean => {
      if ( word.length > 0 && isNaN((word[0] as any) * 1)  && (word[0] === word[0].toUpperCase()) ){
        return true
      }
      return false
    }
*/
const findTarget = (sentence: TText, lexicon: TLexicon) => {
  let output = undefined
      for(let i=0; i < lexicon.words.length ; i++){
        const word = lexicon.words[i].toLocaleLowerCase()
   //     console.log(word, sentence.graphies)
         if( sentence.graphies.includes(word)){
     //     console.log("found")
            output = lexicon.words[i]
         }
      }
  return output
}

@customElement('blank-sentence-exercise')
export class BlankSentenceExercise extends LitElement {


    @property({type: Object})
    sentence: TText | undefined


    @property({type: Object})
    lexicon: TLexicon | undefined

    @state()
    target: string | undefined

    @state()
    parts: string[] | undefined
    
    protected firstUpdated(_changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>): void {
      if (this.sentence && this.lexicon){
      this.target = findTarget(this.sentence, this.lexicon)

        if(this.target && this.sentence.str){
          this.parts = this.sentence?.str.toLocaleLowerCase().split(this.target)
        }
      }
     // console.log("sentence", this.sentence)
     // console.log("lexicon", this.lexicon)
     // console.log("target", this.target)
     // console.log("parts", this.parts)
    }

  handleInput = () => {}

    render(){


     let questionUI

     if(this.target && this.parts){
       questionUI=html`
        <p>${this.parts[0]} <input @input=${this.handleInput} type="text" id="response"> ${this.parts[1]}</p>
       `

     }else{
       questionUI=html`
       <div>${this.sentence?.str}</div>
       <div class="warning">Non valide</div>
       `
      
     }

        return html`
          ${questionUI}

        `
    }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }

      .success {
        color: green;
      }
      .warning {
        color: red;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'blank-sentence-exercise': BlankSentenceExercise
  }
}
