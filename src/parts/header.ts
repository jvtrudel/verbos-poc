import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'


@customElement('header-part')
export class HeaderPart extends LitElement {

  @property({ type: Object})
  state = {}

  navData =  [
    {path: '/', label: 'Accueil'},
 //   {path: '/graphie', label: 'Graphies'},
 //   {path: '/mot', label: 'Mots'},
 //   {path: '/verb-list', label: 'Verb list'},
 //   {path: '/new-verb', label: 'New verb'},
 //   {path: '/exercice-list', label: 'Exercice list'},
 //   {path: '/new-exercice', label: 'New exercice'},
 //   {path: '/view-exercice', label: 'View exercice'},
 //   {path: '/verb-tables', label: 'Verb tables'}
    {path: '/sentence', label: 'Ajout phrase'},
    {path: '/new-lexicon', label: 'Ajout Lexique'},
    {path: '/new-exercice-from-lexicon', label: 'Ajout exercice'},
  ]

  render(){

   const lis=[]

   for (let i = 0; i<this.navData.length; i++){
    lis.push(
        html`
        <li>
          <a href="${this.navData[i].path}">${this.navData[i].label}</a>
        </li>
        `
    )
   }

    return html`
    <header>
    <div class="logo">LOGO</div>
    <nav>
      <ul>
        ${lis}
      </ul>
    </header>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
      header {
        display: flex;
        justify-content: space-between;
      }
      ul {
        list-style-type: none;
        display: flex;
        gap: 1rem;
      }
      a {
        text-decoration: none;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'header-part': HeaderPart
  }
}