import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'


@customElement('aside-part')
export class AsidePart extends LitElement {

  @property({ type: Object})
  state = {}

  navData =  [
    {path: '/', label: 'Accueil'},
   // {path: '/graphie', label: 'Graphies'},
   // {path: '/mot', label: 'Mots'},
   // {path: '/verb-list', label: 'Verb list'},
   // {path: '/new-verb', label: 'New verb'},
   // {path: '/exercice-list', label: 'Exercice list'},
   // {path: '/new-exercice', label: 'New exercice'},
   // {path: '/view-exercice', label: 'View exercice'},
   // {path: '/verb-tables', label: 'Verb tables'}
    {path: '/new-lexicon', label: 'Ajout lexique'},
    {path: '/sentence', label: 'Ajout phrase'},
    {path: '/new-exercice-from-lexicon', label: 'Ajout exercice'},
    {path: '/execute-exercise', label: 'Exercices'},
  ]

  render(){

   const lis=[]

   for (let i = 0; i<this.navData.length; i++){
    lis.push(
        html`
        <li>
          <a href="${this.navData[i].path}">${this.navData[i].label}</a>
        </li>
        `
    )
   }

    return html`
    <aside>
    <nav>
      <ul>
        ${lis}
      </ul>
    </aside>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
      ul {
        list-style-type: none;
      }
      a {
        text-decoration: none;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'aside-part': AsidePart
  }
}