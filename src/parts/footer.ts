import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'


@customElement('footer-part')
export class FooterPart extends LitElement {

  @property({ type: Object})
  state = {}

  render(){

    return html`
    <footer>
    <nav>
    <small>Footer</small>
    </footer>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'footer-part': FooterPart
  }
}