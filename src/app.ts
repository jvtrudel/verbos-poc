export * from './pages'
export * from './parts'
export * from './components'

import { initDatabase, type TDBConfig } from './db/idx'

const dbConfig: TDBConfig = {
    name: "verbDB",
    version: 2,
    collections: [
      {
        name: "exercise",
        keyPath: "meta.uuid"
      },
      {
        name: "exercise-lexicon",
        keyPath: "meta.uuid"
      },
      {
        name: "lexicon",
        keyPath: "meta.uuid"
      },
      {
        name: "sentence",
        keyPath: "meta.uuid"
      },
      {
        name: "mot",
        keyPath: "uuid"
      },
      {
        name: "nom",
        keyPath: "uuid"
      },
      {
        name: "graphie",
        keyPath: "graphie"
      }

    ]
  }
const initApp = async () => {

   initDatabase(dbConfig)
}

initApp()

import {Router} from '@vaadin/router';

const router = new Router(document.getElementById('outlet'));
router.setRoutes([
  {path: '/', component: 'page-accueil'},
//  {path: '/graphie', component: 'page-graphie'},
//  {path: '/mot', component: 'page-mot'},
//  {path: '/verb-list', component: 'page-verb-list'},
//  {path: '/new-verb', component: 'page-new-verb'},
//  {path: '/exercice-list', component: 'page-exercice-list'},
//  {path: '/new-exercice', component: 'page-new-exercice'},
//  {path: '/view-exercice', component: 'page-view-exercice'},
//  {path: '/verb-tables', component: 'page-verb-tables'},
  {path: '/new-lexicon', component: 'page-new-lexicon'},
  {path: '/sentence', component: 'page-manage-sentences'},
  {path: '/new-exercice-from-lexicon', component: 'page-new-exercice-from-lexicon'},
  {path: '/execute-exercise', component: 'page-execute-exercise'},
  {
    path: "(.*)", 
    component: "page-accueil"
  },
]);