import { TExerciceLexicon } from '../types/exercise-lexicon';
import { TEntity } from '../types/entity';
import { getDB } from './idx';

const name = "exercise-lexicon"

export const create = async ( data: TEntity<TExerciceLexicon> ) => {
  await getDB().add(name, data)
}

export const update = async ( data: TEntity<TExerciceLexicon> ) => {
  await getDB().put(name, data)
}

export const list = async (): Promise<TEntity<TExerciceLexicon>[]> => {
  
  // todo: implémente une vérification dans getDB ou une attente juste si ne peut récupérer db
  const promise:Promise<TEntity<TExerciceLexicon>[]>  = new Promise((resolve) => {
    setTimeout(() => {
      resolve(getDB().getAll(name))
    }, 50);
  } )
  return promise
}

export const remove = async (uuid: string ) => {
  await getDB().delete(name, uuid)
  return 
}
