import { getDB } from './idx';

import { TGraphieRefs } from '../types/mot';
//import { TGraphie } from '../types/graphie';

export const pushGraphie = async ( data: TGraphieRefs ) => {

// 1- vérifier si cette graphie existe
    const elem = await getDB().get('graphie', data.graphie)
    console.log(elem)

    //await getDB().add('graphie', data)
}

export const listGraphie = async (): Promise<TGraphieRefs[]> => {
  
  // todo: implémente une vérification dans getDB ou une attente juste si ne peut récupérer db
  const promise:Promise<TGraphieRefs[]>  = new Promise((resolve) => {
    setTimeout(() => {
      resolve(getDB().getAll('graphie'))
    }, 500);
  } )
  return promise
}


// todo: export const getGraphie(graphie: string): Promise<TGraphieRefs>