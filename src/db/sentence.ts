import { TText } from '../types/text';
import { TEntity } from '../types/entity';
import { getDB } from './idx';

const name = "sentence"

export const create = async ( data: TEntity<TText> ) => {
  await getDB().add(name, data)
}

export const update = async ( data: TEntity<TText> ) => {
  await getDB().put(name, data)
}

export const list = async (): Promise<TEntity<TText>[]> => {
  
  // todo: implémente une vérification dans getDB ou une attente juste si ne peut récupérer db
  const promise:Promise<TEntity<TText>[]>  = new Promise((resolve) => {
    setTimeout(() => {
      resolve(getDB().getAll(name))
    }, 50);
  } )
  return promise
}

export const remove = async (uuid: string ) => {
  await getDB().delete(name, uuid)
  return 
}
