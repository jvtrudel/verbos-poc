import { TExerciseForm} from '../types/exercise';
import { TEntity } from '../types/entity';
import { getDB } from './idx';


export const newExercise = async ( data: TEntity<TExerciseForm> ) => {
  await getDB().add('exercise', data)
}

export const listExercise = async (): Promise<TEntity<TExerciseForm>[]> => {
  
  // todo: implémente une vérification dans getDB ou une attente juste si ne peut récupérer db
  const promise:Promise<TEntity<TExerciseForm>[]>  = new Promise((resolve) => {
    setTimeout(() => {
      resolve(getDB().getAll('exercise'))
    }, 500);
  } )
  return promise
}