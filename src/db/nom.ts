import { getDB } from './idx';

//import { TGraphieRefs } from '../types/mot';
//import { TGraphie } from '../types/graphie';
//import { TMot } from '../types/mot';

import { TNom } from '../types/mot';
import { NomVersGraphies } from '../libs/nom';


export const pushNom = async ( data: TNom ) => {

// 1- vérifier si ce mot existe

   const graphies = NomVersGraphies(data)
   console.log("pushNom")
   console.log(data)
   console.log(graphies)

  //  await getDB().add('nom', data)

  // 2- pousser toutes les graphies

}

export const listNom = async (): Promise<TNom[]> => {
  
  // todo: implémente une vérification dans getDB ou une attente juste si ne peut récupérer db
  const promise:Promise<TNom[]>  = new Promise((resolve) => {
    setTimeout(() => {
      resolve(getDB().getAll('nom'))
    }, 500);
  } )
  return promise
}


// todo: export const getGraphie(graphie: string): Promise<TGraphieRefs>