import { TLexicon } from '../types/lexicon';
import { TEntity } from '../types/entity';
import { getDB } from './idx';


export const create = async ( data: TEntity<TLexicon> ) => {
  await getDB().add('lexicon', data)
}

export const update = async ( data: TEntity<TLexicon> ) => {
  await getDB().put('lexicon', data)
}

export const list = async (): Promise<TEntity<TLexicon>[]> => {
  
  // todo: implémente une vérification dans getDB ou une attente juste si ne peut récupérer db
  const promise:Promise<TEntity<TLexicon>[]>  = new Promise((resolve) => {
    setTimeout(() => {
      resolve(getDB().getAll('lexicon'))
    }, 50);
  } )
  return promise
}

export const remove = async (uuid: string ) => {
  await getDB().delete('lexicon', uuid)
  return 
}
