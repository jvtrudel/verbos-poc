import {openDB } from 'idb';

export type TCollectionConfig = {
  name: string
  keyPath: string
}

export type TDBConfig = {
    name: string,
    version: number,
    collections: TCollectionConfig[]


}



let db: any

export const initDatabase = async (config: TDBConfig) => {
    // todo: appliquer la migration lorque la version de BD change
    db = await openDB (
          config.name,
          config.version,{
            upgrade(db: any) {
              for(let i = 0; i< config.collections.length; i++){
                const col = config.collections[i]
                if ( ! db.objectStoreNames.contains(col.name) ){
                    const store = db.createObjectStore(col.name, { keyPath: col.keyPath, autoIncrement: false})
                    store.createIndex(col.name, col.keyPath, {unique: true})
                }             
              }
            }
          }
        )
}

export const getDB = () => {return db };