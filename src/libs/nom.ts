
import { TNom } from "../types/mot"
import { TGraphieRefs } from "../types/mot"
import { TGenre, TNombre } from "../types/mot"


export const NomVersGraphies = (nom: TNom): TGraphieRefs[] => {
  return [
    { graphie: nom.masc_sing, mots: [ { uuid: nom.uuid , genre: TGenre.masc, nombre: TNombre.sing}] },
    { graphie: nom.masc_plur, mots: [ { uuid: nom.uuid , genre: TGenre.masc, nombre: TNombre.plur}] },
    { graphie: nom.fem_sing, mots: [ { uuid: nom.uuid , genre: TGenre.fem, nombre: TNombre.sing}] },
    { graphie: nom.fem_plur, mots: [ { uuid: nom.uuid , genre: TGenre.fem, nombre: TNombre.plur}] }
  ]
}

