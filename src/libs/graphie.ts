import { TGraphieRefs } from "../types/mot"

export const graphieCombine = (graphie1: TGraphieRefs, graphie2: TGraphieRefs ): TGraphieRefs[] => {

    if ( graphie1.graphie === graphie2.graphie ){
        return [ 
            { graphie: graphie1.graphie , mots: graphie1.mots.concat(graphie1.mots) }
         ]
    }else{
        return [ graphie1, graphie2 ]
    }
}

export const graphiePush = (graphieArray: TGraphieRefs[], graphieItem: TGraphieRefs ): TGraphieRefs[] => {
    const out = []
    let merged = false
    for(let i= 0 ; i < graphieArray.length; i++){
      const graphie0= graphieArray[i]
      if ( graphie0.graphie !== graphieItem.graphie ){
        out.push(graphieItem)
      }else{
        merged = true
        const mGraphieArray = graphieCombine(graphie0,graphieItem)
        out.push(mGraphieArray[0])
      }
    }
    if (! merged){
        out.push(graphieItem)
    }
    return out
}

export const graphieConcat = (graphies1: TGraphieRefs[], graphies2: TGraphieRefs[]): TGraphieRefs[] => {
    let out = graphies1
    for(let i = 0; i< graphies2.length; i++){
       out = graphiePush(out,graphies2[i])
    }
    return out
}