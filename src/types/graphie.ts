export type TGraphie = {
  mot_uuid?: string
  graphie: String
}

export type TTokenGraphie = {
    position: number
    lettre: string
    accent: boolean
    majuscule: boolean
}


export type TGraphieEtendue = TGraphie & {
    syllabes: String[]
    racine: string
    terminaison: string
    tokens: TTokenGraphie[]
}