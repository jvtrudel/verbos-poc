export type TExerciseHeader = {
    title: String
    description?: String
    goal?: String
    category?: String
}

export type TExerciseHints = {
      verb?: Boolean
      tense?: Boolean
      success?: Boolean
}

export type TExerciseContent = {
   statement: String
   verb: String
   tense: String
}


export type TExerciseForm = {
    header: TExerciseHeader
    content: TExerciseContent
    hints: TExerciseHints
}
