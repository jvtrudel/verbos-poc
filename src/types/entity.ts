export type TEntity<entityType> = TEntityRef & entityType


export type TEntityRef = {
  meta: { 
    uuid: string
  }
}