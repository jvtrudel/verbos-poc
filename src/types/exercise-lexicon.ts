import { TEntity } from "./entity"
import { TText } from "./text"
import { TLexicon } from "./lexicon"

export type TExerciceLexicon = {
    title: string
    lexicon: TEntity<TLexicon>
    sentences: TEntity<TText>[]
}