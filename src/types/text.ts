
export type TText = {
  str: string,
  graphies: string[],
  wordCount: number,
  charCount: number 
}

// https://stackoverflow.com/questions/9229645/remove-duplicate-values-from-js-array
function uniq(a: any) {
    return a.sort().filter(function(item: any, pos: any, ary: any) {
        return !pos || item != ary[pos - 1];
    });
}

export const stringToSentence = (str: string): TText => {
  const regex=/[\t\s.,:;'!?¡¿]/
  const graphies = str.toLocaleLowerCase().split(regex).filter(e => e !== "" )
  const chars = str.split("").length
  return {
    str,
    graphies: uniq(graphies),
    wordCount: graphies.length,
    charCount: chars
  }
}