export type TNom = {
    label?: string
    uuid: string
    masc_sing: string
    masc_plur: string
    fem_sing: string
    fem_plur: string
}

export type TMot = TNom

export enum TGenre {
  masc= "masc",
  fem= "fem",
  neutre= "neutre"
}

export enum TNombre {
    sing= "sing",
    plur= "plur",
    invariable = "invariable"
}

export type TGraphieRefs= {
    graphie: string
    mots:
       { uuid: string,
        genre: TGenre,
        nombre: TNombre
       }[]
}

