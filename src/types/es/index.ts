// Référence: Bescherelle espagnol, édiiton 1998. 
//            ISBN 2-89428-332-6 (Hurtubise)
//            ISBN 2-218-72542-8 (Hatier)

// todo: évaluer la pertinence d'utiliser valibot.dev





// liste les formes de verbes
export enum VerbForms {
  regular = "regular"
}

// liste les terminaisons de l'infinitif
export enum VerbTerms {
    ar = "ar",
    er = "er",
    ir = "ir"
}

// todo: étendre la classification des temps de verbe =>
//         - formas (personales, no personales),
//         - modo (indicativo, subjunctivo, imperativo)
//         - tiempos (simples, compuestos)
//         - ??? (presete, imperfecto, ..., futuro, condicional)
//         - nombre: singulier
//         - personne: 1, 2, 3
//         - genre: masculin, féminin, neutre, ...  
//       { formas, modo, tiempo, ???, nombre, personne, genre}
export enum VerbTimes {
    // modo indicativo simples
    presente,
    preterito_imperfecto,
    preterito_perfecto_simple,
    futuro,
    condicional,
    // modo indicativo 
    protorito_perfecto_compuesto,
    preterito_pluscuamperfecto,
    preterito_anterior,
    futuro_perfecto,
    // ...
}

export enum VerbPronouns {
    yo       = "yo",
    tu       = "tu",
    el       = "el",
    ella     = "ella",
    usted    = "usted",
    nosotros = "nosotros",
    vosotros = "vosotros",
    ellos = "ellos",
    ellas = "ellas",
    ustedes = "ustedes"
}


export type TVerbExpression = {
    time: VerbTimes
    pronoun?: VerbPronouns,
    verb: String
}

// Primitives d'un verbe régulier
export type TRegularVerbPrim = {
    term: VerbTerms
    root: String
}

// Informations minimales d'un verbe régulier
export type TRegularVerbMinimal = TRegularVerbPrim & {
   infinitif: String
}


export type TRegularVerbExpanded = TRegularVerbMinimal & {

verbExpressions: TVerbExpression[] 
}


// Représentation en terme de primitive
export type TVerbPrim = {
    form: VerbForms
} & TRegularVerbPrim

// Représentation contenant quelques informations utiles
export type TVerbMinimal = TRegularVerbPrim


// Représentation étendu permettant de 
export type TVerbExpanded = TRegularVerbExpanded 


// Tout verbe peut être décrit par l'une ou l'autre des ses représentation 
export type TVerb =  TVerbPrim | TVerbMinimal | TVerbExpanded


export type TVerbEntity = {
    verb: TVerb
    uuid: String
    meta?: Object
    history?: Object
    annotations?: Object
    authorRef?: Object
    source?: Object 
}


