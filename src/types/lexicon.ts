export type TLexicon = {
  title: string
  words: string[]
}