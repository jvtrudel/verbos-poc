import { LitElement, PropertyValueMap, css, html } from 'lit'
import { customElement, property, query } from 'lit/decorators.js'

import { TEntity } from '../types/entity'
import { TText, stringToSentence } from '../types/text'

import { update, list, remove } from '../db/sentence'


import { v4 as uuidv4 } from 'uuid'





@customElement('page-manage-sentences')
export class PageManageSentences extends LitElement {


  @property({ type: Object})
  sentence: TEntity<TText> = {
     ...(stringToSentence("")),
    meta: {
      uuid: uuidv4()
    } }

  @property({type: Object})
  db: TEntity<TText>[] = []


  @query("#sentence")
  sentenceQ: unknown


  protected async firstUpdated(_changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>): Promise<void> {
    //alert(this.lexicon.meta.uuid)
    this.db = await list ()
    if (this.db.length > 0){
      this.sentence = this.db[0]
    }
  }


  switchLexicon = ( uuid: string) => {
    for(let i= 0; i < this.db.length; i++){
      if( uuid === this.db[i].meta.uuid){
        this.sentence = this.db[i]
      }
    }
  }

  syncSelector = async () => {
    this.db = await list()
  }



  handleSubmit = (e: Event) => {
    //alert("submit")
    e.preventDefault()
    this.sentence = { 
      ...(stringToSentence((this.sentenceQ as HTMLInputElement).value)),
      meta: this.sentence.meta
    }
    update(this.sentence)
    this.syncSelector()

  }

  handleSelectLexicon =  (ev: CustomEvent<string>) => {
    const selected = ev.detail
    this.switchLexicon(selected)
  }

  handleDeleteLexicon = async () => {
   remove( this.sentence.meta.uuid )
    this.sentence = {
     ...(stringToSentence("")),
    meta: {
      uuid: uuidv4()
    } }
    
   this.db = await list()


  }

  handleNewLexicon = () => {
    this.sentence = {
     ...(stringToSentence("")),
    meta: {
      uuid: uuidv4()
    } }
    update(this.sentence)
    this.syncSelector()
  }

  render(){
    //alert(JSON.stringify(this.sentence))
    const lexiconUI = html`
    <fieldset>
      <legend></legend>
       <label for="sentence">Phrase</label>
       <textarea id="sentence">${this.sentence.str}</textarea>
       </br>
       <small>${this.sentence.meta.uuid}</small>
    </fieldset>
    `

    return html`



    <page-template>

    <select-sentence @select=${this.handleSelectLexicon} .items=${this.db} .selected=${this.sentence.meta.uuid}> </select-sentence>
    </br>
    <button @click=${this.handleNewLexicon}> Ajoute une phrase </button>

    <form>
    ${lexiconUI}
    <button @click=${this.handleSubmit}>Enregistrer</button> <button @click=${this.handleDeleteLexicon}>supprimer</button>
    </form >
  

    </page-template>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }


       fieldset {
        margin: 2em 0;
        padding: 1em 2em;
        border: solid 1px #ccc;
        border-radius: 6px;
        min-width: 200px;
      }
      legend {
        fon-size: 1.25em;
        padding: 0 .5em;
        color: #999;
      }
      /* Labels */
      label {
        display: block;
        margin-top: 1em;
      }
      .checks label {
        margin-top: 0;
      }
      label:first-of-type {
        margin-top: 0;
      }

      /* Inputs and textarea */
      input {
        padding: .5em;
        border: solid 1px #999;
      }

      input[type="text"] {
        width: 25em;
      }
      textarea {
        min-height: 8em;
        min-width: 90%;
        padding: .5em;
        border: solid 1px #999;
      }

      /* checkboxes */


      input[type="checkbox"] + label {
        display: inline-block;
        padding-top: 0;
        margin-top: 0;
      }
      input[type="checkbox"] { 
        margin-left: 1.5em;
      }
      input[type="checkbox"]:first-of-type { 
        margin-left: 0em;
      }

      /* button */
      input[type="submit"]{
        padding: .5em 1em;
        border-radius: 6px;
        background-color: #333;
        color: #fff;
        font-size: .8em;
      }


    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-manage-sentences': PageManageSentences
  }
}