import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'


@customElement('page-verb-tables')
export class PageVerbTables extends LitElement {

  @property({ type: Object})
  state = {}

  render(){
    return html`
    <page-template>
    
    <h2>Verbo regular</h2>
    <es-regular-verb verb='{ "infinitivo": "comer", "raiz": "com", "terminacion": "er"}'></es-regular-verb>
    <es-regular-verb verb='{ "infinitivo": "vivir", "raiz": "viv", "terminacion": "ir"}'></es-regular-verb>
    <es-regular-verb verb='{ "infinitivo": "cortar", "raiz": "cort", "terminacion": "ar"}'></es-regular-verb>

    </page-template>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-verb-tables': PageVerbTables
  }
}