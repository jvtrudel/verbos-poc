import { LitElement, css, html } from 'lit'
import { customElement, query, state, property } from 'lit/decorators.js'

import { v4 as uuidv4 } from 'uuid'

import { TNom } from '../types/mot'
import { pushNom } from '../db/nom'



@customElement("formulaire-nom-irregulier")
export class FormulaireNomIrregulier extends LitElement {


  @property({type: Object})
  value: TNom | undefined
  
  uuid: string = uuidv4()

  @query("#masc_sing")
  mascSingQ: unknown

  @query("#masc_plur")
  mascPlurQ: unknown

  @query("#fem_sing")
  femSingQ: unknown

  @query("#fem_plur")
  femPlurQ: unknown

  handleInput = (ev: Event) => {
    ev.stopPropagation()
    const data = {
      uuid: this.uuid,
      masc_sing: (this.mascSingQ as HTMLInputElement).value,
      masc_plur: (this.mascPlurQ as HTMLInputElement).value,
      fem_sing: (this.femSingQ as HTMLInputElement).value,
      fem_plur: (this.femPlurQ as HTMLInputElement).value
    }
    console.log(data)
    const event = new CustomEvent("input", { detail: data });
    this.dispatchEvent(event)
  }

  render(){

    return html`
      <fieldset>
        <legend>Nom</legend>

        <label for="masc_sing">Masculin Singulier</label>
        <input type="text" id="masc_sing" @input=${this.handleInput}/>

        <label for="fem_sing">Féminin Singulier</label>
        <input type="text" id="fem_sing" @input=${this.handleInput}/>

        <label for="masc_plur">Masculin Pluriel</label>
        <input type="text" id="masc_plur" @input=${this.handleInput}/>

        <label for="fem_sing">Féminin Pluriel</label>
        <input type="text" id="fem_plur" @input=${this.handleInput}/>

      </fieldset>
    `

  }

}



@customElement('page-mot')
export class PageMot extends LitElement {

  data: TNom | undefined 

  @state()
  selected: string = "inconnu"

  @query("#selected")
  selectedQ: unknown

  handleInput = () => {
  }

  handleSelect = () => {
    this.selected = (this.selectedQ as HTMLSelectElement).value
    console.log(this.selected)
  }

  handleInputFormulaire = (ce: CustomEvent<TNom>) => {
    ce.preventDefault()
    this.data = ce.detail
    console.log("input formulaire:", ce.detail)
  }

  handleSubmit = (ev: Event) => {
    ev.preventDefault()

    

   console.log("handle submit")
   console.log(this.data)
   // todo validate data is TName
   pushNom((this.data as TNom))

  }

  render(){

console.log("rerender")
    let typeForm

    if(this.selected === "nom"){
      typeForm = html`
      <formulaire-nom-irregulier id="formulaire" @input=${this.handleInputFormulaire}>
      </formulaire-nom-irregulier>
      `
    }

    return html`
    <page-template>
    <h1>Mot</h1>
    <small>Définit les différentes graphies d'un mot</small>
   
     <form @submit=${this.handleSubmit}>
       <label for="mot">Mot</label>
       <input type="text" id="mot" @input=${this.handleInput}/>
       <select id="selected" @input=${this.handleSelect}>
         <option value="inconnu">inconnu</option>
         <option value="verbe">verbe</option>
         <option value="nom">nom</option>
         <option value="pronom">pronom</option>
         <option value="adjectif">adjectif</option>
         <option value="adverbe">adverbe</option>
       </select>

       ${typeForm}

       <input type="submit" value="submit"/>
       </form>

       


      <div>
       type: ${this.selected}
      </div>


    </page-template>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
        width: 90%;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-mot': PageMot,
    'formulaire-nom-irregulier': FormulaireNomIrregulier,
  }
}