import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'

@customElement('page-verb-list')
export class PageVerbList extends LitElement {

  @property({ type: Object})
  state = {}

  render(){
    return html`
    <page-template> 
    <h1>Verb List</h1> 
    </page-template>   
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-verb-list': PageVerbList
  }
}