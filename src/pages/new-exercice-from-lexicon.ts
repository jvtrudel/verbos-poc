import { LitElement, PropertyValueMap, css, html } from 'lit'
import { customElement, property, query } from 'lit/decorators.js'

import { TEntity } from '../types/entity'
import { TLexicon } from '../types/lexicon'

import { list as listLexicon} from '../db/lexicon'
import { list as listSentence } from '../db/sentence'
import { update } from '../db/exercice-lexicon'


import { v4 as uuidv4 } from 'uuid'
import { TText } from '../types/text'


@customElement('page-new-exercice-from-lexicon')
export class PageNewExerciceFromLexicon extends LitElement {


  @property({ type: Object})
  lexicon: TEntity<TLexicon> | undefined

  @property({type: String})
  title = "Nouvel exercice"

  @property({ type: Object})
  sentences: TEntity<TText>[] = []

  @property({type: String})
  selectedSentence= ""



  @property({type: Object})
  dbLexicons: TEntity<TLexicon>[] = []

  @property({type: Object})
  dbSentences: TEntity<TText>[] = []

  @query("#title")
  titleQ: unknown

  @query("#lexicon-words")
  lexiconWordsQ: unknown


  protected async firstUpdated(_changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>): Promise<void> {
    this.dbLexicons = await listLexicon ()
    if (this.dbLexicons.length > 0){
      this.lexicon = this.dbLexicons[0]
    }
    
    this.dbSentences = await listSentence ()
    if (this.dbSentences.length > 0){
      this.selectedSentence = this.dbSentences[0].meta.uuid
    }
  }


  switchLexicon = ( uuid: string) => {
    for(let i= 0; i < this.dbLexicons.length; i++){
      if( uuid === this.dbLexicons[i].meta.uuid){
        this.lexicon = this.dbLexicons[i]
      }
    }
  }

  handleSelectLexicon =  (ev: CustomEvent<string>) => {
    const selected = ev.detail
    this.switchLexicon(selected)
  } 



  handleChooseSentence = () => {
    const out = [ ...this.sentences]
    for( let i = 0; i < this.dbSentences.length ; i++){
      if( this.selectedSentence === this.dbSentences[i].meta.uuid){
        out.push(
          this.dbSentences[i]
        )
      }
    }
    this.sentences = out
  }

  handleSelectSentence = (ev: CustomEvent<string>) => {
    this.selectedSentence = ev.detail
     
  }

  handleSaveExercise = () => {

    
    update(
      {meta: {
        uuid: uuidv4()
      },
      title: (this.titleQ as HTMLInputElement).value,
      lexicon: (this.lexicon as TEntity<TLexicon>),
      sentences: this.sentences
    }

    )

  }

  render(){

//    <input-sentence .lexicon=${this.lexicon} ></input-sentence>

    const sentences = []

    for (let i = 0; i < this.sentences.length ; i++){
      sentences.push(
        html`
          <li>${this.sentences[i].str}</li>
        `
      )
    }
    const formUI = html`
      <fieldset>
        <legend>Exercice</legend>
        <label for="title">Titre</label>
        <input id="title" .value=${this.title} />
        <ul>
        ${sentences}
        </ul>
      </fieldset>
    `


    return html`

    <page-template>


    <select-lexicon .lexicons=${this.dbLexicons} @select=${this.handleSelectLexicon}></select-lexicon>
    </br>
    <small>${this.lexicon && this.lexicon.words.join(", ")}</small>
    <div>
    
    <select-sentence .items=${this.dbSentences} @select=${this.handleSelectSentence}></select-sentence>
    <button @click=${this.handleChooseSentence}>Ajouter cette phrase</button>
    ${formUI}
    <button @click=${this.handleSaveExercise}>Saugevarder cet exercice</button>
    </div>
    </page-template>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }


       fieldset {
        margin: 2em 0;
        padding: 1em 2em;
        border: solid 1px #ccc;
        border-radius: 6px;
        min-width: 200px;
      }
      legend {
        fon-size: 1.25em;
        padding: 0 .5em;
        color: #999;
      }
      /* Labels */
      label {
        display: block;
        margin-top: 1em;
      }
      .checks label {
        margin-top: 0;
      }
      label:first-of-type {
        margin-top: 0;
      }

      /* Inputs and textarea */
      input {
        padding: .5em;
        border: solid 1px #999;
      }

      input[type="text"] {
        width: 25em;
      }
      textarea {
        min-height: 8em;
        min-width: 90%;
        padding: .5em;
        border: solid 1px #999;
      }

      /* checkboxes */


      input[type="checkbox"] + label {
        display: inline-block;
        padding-top: 0;
        margin-top: 0;
      }
      input[type="checkbox"] { 
        margin-left: 1.5em;
      }
      input[type="checkbox"]:first-of-type { 
        margin-left: 0em;
      }

      /* button */
      input[type="submit"]{
        padding: .5em 1em;
        border-radius: 6px;
        background-color: #333;
        color: #fff;
        font-size: .8em;
      }


    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-new-exercice-from-lexicon': PageNewExerciceFromLexicon
  }
}