import { LitElement, PropertyValueMap, css, html } from 'lit'
import { customElement, property, query } from 'lit/decorators.js'

import { TEntity } from '../types/entity'
import { TLexicon } from '../types/lexicon'

import { update, list, remove } from '../db/lexicon'


import { v4 as uuidv4 } from 'uuid'





@customElement('page-new-lexicon')
export class PageNewLexicon extends LitElement {


  @property({ type: Object})
  lexicon: TEntity<TLexicon> = {
    meta: { uuid:  uuidv4() },
    title: "New Lexicon",
    words: []
  }

  @property({type: Object})
  dbLexicons: TEntity<TLexicon>[] = []


  @query("#lexicon-title")
  lexiconTitleQ: unknown

  @query("#lexicon-words")
  lexiconWordsQ: unknown


  protected async firstUpdated(_changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>): Promise<void> {
    //alert(this.lexicon.meta.uuid)
    this.dbLexicons = await list ()
    if (this.dbLexicons.length > 0){
      this.lexicon = this.dbLexicons[0]
    }
  }


  switchLexicon = ( uuid: string) => {
    for(let i= 0; i < this.dbLexicons.length; i++){
      if( uuid === this.dbLexicons[i].meta.uuid){
        this.lexicon = this.dbLexicons[i]
      }
    }
  }

  syncSelector = async () => {
    this.dbLexicons = await list()
  }



  handleSubmitLexicon = (e: Event) => {
    //alert("submit")
    e.preventDefault()
    this.lexicon = { 
      ...this.lexicon, 
      title: ((this.lexiconTitleQ as HTMLInputElement).value).trim(),
      words: ((this.lexiconWordsQ as HTMLTextAreaElement).value).split("\n")
    }
    update(this.lexicon)
    this.syncSelector()

  }

  handleSelectLexicon =  (ev: CustomEvent<string>) => {
    const selected = ev.detail
    this.switchLexicon(selected)
  }

  handleDeleteLexicon = async () => {
   remove( this.lexicon.meta.uuid )
    this.lexicon = {
    meta: { uuid:  uuidv4() },
    title: "New Lexicon",
    words: []
  }
   this.dbLexicons = await list()


  }

  handleNewLexicon = () => {

    this.lexicon = {
    meta: { uuid:  uuidv4() },
    title: "New Lexicon",
    words: []
  }
  }

  render(){

    const lexiconUI = html`
    <fieldset>
      <legend>Lexique</legend>
       <label for="lexicon-title">Titre</label>
       <input type="text" id="lexicon-title" .value="${this.lexicon.title}"/>
       <label for="lexicon-words">Mots</label>
       <textarea id="lexicon-words">${this.lexicon.words.join("\n")}</textarea>
       </br>
       <small>${this.lexicon.meta.uuid}</small>
    </fieldset>
    `

    return html`



    <page-template>

    <select-lexicon @select=${this.handleSelectLexicon} .lexicons=${this.dbLexicons} .selected=${this.lexicon.meta.uuid}> </select-lexicon>
    <button @click=${this.handleNewLexicon}> Ajoute un lexique </button>

    <form>
    ${lexiconUI}
    <button @click=${this.handleSubmitLexicon}>Enregistrer</button> <button @click=${this.handleDeleteLexicon}>supprimer</button>
    </form >
  

    </page-template>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }


       fieldset {
        margin: 2em 0;
        padding: 1em 2em;
        border: solid 1px #ccc;
        border-radius: 6px;
        min-width: 200px;
      }
      legend {
        fon-size: 1.25em;
        padding: 0 .5em;
        color: #999;
      }
      /* Labels */
      label {
        display: block;
        margin-top: 1em;
      }
      .checks label {
        margin-top: 0;
      }
      label:first-of-type {
        margin-top: 0;
      }

      /* Inputs and textarea */
      input {
        padding: .5em;
        border: solid 1px #999;
      }

      input[type="text"] {
        width: 25em;
      }
      textarea {
        min-height: 8em;
        min-width: 90%;
        padding: .5em;
        border: solid 1px #999;
      }

      /* checkboxes */


      input[type="checkbox"] + label {
        display: inline-block;
        padding-top: 0;
        margin-top: 0;
      }
      input[type="checkbox"] { 
        margin-left: 1.5em;
      }
      input[type="checkbox"]:first-of-type { 
        margin-left: 0em;
      }

      /* button */
      input[type="submit"]{
        padding: .5em 1em;
        border-radius: 6px;
        background-color: #333;
        color: #fff;
        font-size: .8em;
      }


    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-new-lexicon': PageNewLexicon
  }
}