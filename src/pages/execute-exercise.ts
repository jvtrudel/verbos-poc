import { LitElement, PropertyValueMap, css, html } from 'lit'
import { customElement, property, query } from 'lit/decorators.js'

import { TEntity } from '../types/entity'
import { TExerciceLexicon } from '../types/exercise-lexicon'

import { list } from '../db/exercice-lexicon'
//import { list as listSentence } from '../db/sentence'
//import { update } from '../db/exercice-lexicon'


import { TText } from '../types/text'


@customElement('page-execute-exercise')
export class PageExecuteExercise extends LitElement {


  @property({ type: Object})
  exercise: TEntity<TExerciceLexicon> | undefined

  @property({type: String})
  title = "Nouvel exercice"

  @property({ type: Object})
  sentences: TEntity<TText>[] = []

  @property({type: String})
  selectedSentence= ""



  @property({type: Object})
  db: TEntity<TExerciceLexicon>[] = []

  @property({type: Object})
  dbSentences: TEntity<TText>[] = []

  @query("#title")
  titleQ: unknown

  @query("#lexicon-words")
  lexiconWordsQ: unknown


  protected async firstUpdated(_changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>): Promise<void> {
    this.db = await list ()
    if (this.db.length > 0){
      this.exercise = this.db[0]
    }
  }


  switchExercise = ( uuid: string) => {

    for(let i= 0; i < this.db.length; i++){
      if( uuid === this.db[i].meta.uuid){
      //  alert("changed")
        this.exercise = this.db[i]
      }
    }
    //alert(JSON.stringify(this.exercise))
  }

  handleSelectExercise =  (ev: CustomEvent<string>) => {
    const selected = ev.detail
    this.switchExercise(selected)
  } 

  render(){

//    <input-sentence .lexicon=${this.lexicon} ></input-sentence>

    const sentences = []

    if(this.exercise && this.exercise.sentences){
      for (let i = 0; i < this.exercise.sentences.length ; i++){
       // alert(JSON.stringify(this.exercise.sentences[i]))
        sentences.push(
          html`
          <li>
            <blank-sentence-exercise .sentence=${this.exercise.sentences[i]} .lexicon=${this.exercise.lexicon} > </blank-sentence-exercise>
          </li>
          `
        )
      }
    }
//    console.log(sentences)
    const exerciceUI = html`
      <div>
        <h1>${this.exercise && this.exercise.title}</h1>
        <ul>
        ${sentences}
        </ul>
      </div>
    `


    return html`

    <page-template>

    <select-exercise-lexicon .items=${this.db} @select=${this.handleSelectExercise}></select-exercise-lexicon>
    ${exerciceUI}
    </page-template>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }


       fieldset {
        margin: 2em 0;
        padding: 1em 2em;
        border: solid 1px #ccc;
        border-radius: 6px;
        min-width: 200px;
      }
      legend {
        fon-size: 1.25em;
        padding: 0 .5em;
        color: #999;
      }
      /* Labels */
      label {
        display: block;
        margin-top: 1em;
      }
      .checks label {
        margin-top: 0;
      }
      label:first-of-type {
        margin-top: 0;
      }

      /* Inputs and textarea */
      input {
        padding: .5em;
        border: solid 1px #999;
      }

      input[type="text"] {
        width: 25em;
      }
      textarea {
        min-height: 8em;
        min-width: 90%;
        padding: .5em;
        border: solid 1px #999;
      }

      /* checkboxes */


      input[type="checkbox"] + label {
        display: inline-block;
        padding-top: 0;
        margin-top: 0;
      }
      input[type="checkbox"] { 
        margin-left: 1.5em;
      }
      input[type="checkbox"]:first-of-type { 
        margin-left: 0em;
      }

      /* button */
      input[type="submit"]{
        padding: .5em 1em;
        border-radius: 6px;
        background-color: #333;
        color: #fff;
        font-size: .8em;
      }


    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-execute-exercise': PageExecuteExercise
  }
}