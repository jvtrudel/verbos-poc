import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'

@customElement('page-accueil')
export class PageAccueil extends LitElement {

  @property({ type: Object})
  state = {}

  render(){
    return html`
    <page-template>
    <h1>Accueil</h1>
    </page-template>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
        width: 90%;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-acceuil': PageAccueil
  }
}