import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'

@customElement('page-template')
export class PageTemplate extends LitElement {

  @property({ type: Object})
  state = {}

  render(){
    return html`
    <div class="out-container">
    <header-part state=${this.state}></header-part>
    <div class="container">
      <aside-part state=${this.state}></aside-part> 
      <main>
      <slot></slot>
      </main>
    </div>
    <footer-part ${this.state}></footer-part>
    <div>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
        width: 100%;
      }
      .out-container {
        margin: 1rem 2rem;
      }
      .container {
        display: grid;
        grid-template-columns: 1fr 4fr;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-template': PageTemplate
  }
}