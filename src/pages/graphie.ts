import { LitElement, css, html } from 'lit'
import { customElement, query, state } from 'lit/decorators.js'

  const voyelles = [
    "a",
    "á",
    "e",
    "é",
    "i",
    "í",
    "o",
    "ó",
    "u",
    "ú",
    "y",
  ] 

  const consonnes= [
    "b",
    "c",
    "d",
    "f",
    "g",
    "h",
    "j",
    "k",
    "l",
    "ll",
    "m",
    "n",
    "ñ",
    "p",
    "q",
    "r",
    "s",
    "t",
    "v",
    "w",
    "x",
    "z",
  ]

const estConsonne = (l: string): Boolean => {
  return consonnes.includes(l)
}

const estVoyelle= (l: string): Boolean => {
  return voyelles.includes(l)
}



@customElement('page-graphie')
export class PageGraphie extends LitElement {

  @query("#graphieI")
  graphieQ: unknown

  @state()
  graphie: string = ""

  @state()
  syllabes: string[] = []

  @state()
  racine: string = ""

  @state()
  terminaison: string = ""

  @state()
  nLettres: number= 0



  handleSubmitGraphie = () => {

    // todo: vérifie que la graphie est répertorié

  }


  handleInput = () => {
   // décompose en syllabes
  
   // todo: comment on découppe en syllabes en espagnol?
   // todo: fonction to graphieEtendue
    console.log("handleInput")
    const syllabes = []
    this.graphie =  (this.graphieQ as HTMLInputElement).value.trim()
    const lettres = this.graphie.split("")

  //  const tokens = []
    let syllabe = ""
    let j = 0
    for(let i = 0; i< lettres.length; i++){
      let lettre = lettres[i]
        
      if (lettre === "l"){
        if(lettres[i+1] === "l"){
          lettre = lettre + "l"
          i++
        }
      }

      if(i === 0 || estVoyelle(lettre) || i === lettres.length -1){
        syllabe= syllabe + lettre
      }else if (estConsonne(lettre) && ! estConsonne(lettres[i+1]) ){
        syllabes.push(syllabe)
        syllabe = lettre
      }else if(estConsonne(lettre) && estConsonne(lettres[i+1]) )
        syllabe = syllabe + lettre
      j++      
    }
   syllabes.push(syllabe)
   this.syllabes = syllabes

   this.terminaison = syllabes[syllabes.length - 1]
   const tmp = [...syllabes]
   tmp.pop()
   this.racine = tmp.join("")    
  
   // todo: transforme en TTokenGraphie[]
   // "emmerdement": ll compte pour une lettre en espagnol
  }



  render(){

    const syllabes = []

    for(let i = 0; i<this.syllabes.length; i ++){
      syllabes.push(
        html`<li>${this.syllabes[i]}</li>`
      )
    }

    return html`
    <page-template>
    <h1>Graphies</h1>
    <small>
      Une graphie est une forme de mot. La graphie est une variation d'un mot en fonction du context.
      Par exemple, pour les noms, un mot s'écrit différemment selon son nombre et son genre.
    </small>

    <form @submit=${this.handleSubmitGraphie}>

      <fieldset>
        <legend>Mot</legend>

        <label id="graphie">graphie</label>
        <input type=text" id="graphieI" for="graphie" @input=${this.handleInput}/>
        <input type="submit" 
      </fieldset>
    </form>

    <div>
      <div>Graphie: ${this.graphie}</div>
      <div>Syllabes</div>
       <ul>
       ${syllabes}
       </ul>
      <div>Racine ${this.racine}</div>
      <div>terminaison: ${this.terminaison}</div>
      <div>Nombre de lettres: ${this.graphie.length}</div>
      <div>Tokenisation</div>
    </div>

    </page-template>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
        width: 90%;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-graphie': PageGraphie
  }
}