import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'

@customElement('page-new-verb')
export class PageNewVerb extends LitElement {

  @property({ type: Object})
  state = {}

  render(){
    return html`
    <page-template>
    <h1>New Verb</h1>
    </page-template>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-new-verb': PageNewVerb
  }
}