import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'

import { TEntity } from '../types/entity'
import { TExerciseForm} from '../types/exercise'
import { newExercise } from '../db/exercise'

@customElement('page-new-exercice')
export class PageNewExercice extends LitElement {

  @property({ type: Object})
  state: TExerciseForm = {
        header: {
          title: 'New Exercise',
          description: "Exercise descripiton",
          goal: "Educational goal",
          category: "easy"
        },
        content: {
          statement: "Exercice statement",
          verb: "comer",
          tense: "presente"
        },
        hints: {
          verb: false,
          tense: false,
          success: false
        }
  }


  handleExerciseSubmitted = (e: CustomEvent<TEntity<TExerciseForm>>) => {
     newExercise(e.detail)
  }


  render(){
    return html`
    <page-template>
    <new-exercise
      @submit=${this.handleExerciseSubmitted}
      content="${this.state.content}"
      header="${this.state.header}"
      clues="${this.state.hints}"
       ></new-exercise> 
    </page-template>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-new-exercice': PageNewExercice
  }
}