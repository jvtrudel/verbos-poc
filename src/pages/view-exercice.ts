import { LitElement, PropertyValueMap, css, html } from 'lit'
import { customElement, state} from 'lit/decorators.js'
import { listExercise } from '../db/exercise'
import { TEntity } from '../types/entity';
import { TExerciseForm } from '../types/exercise';


@customElement('page-view-exercice')
export class PageViewExercice extends LitElement {


  @state()
  exercises: TEntity<TExerciseForm>[] = []

  protected async firstUpdated(_changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>): Promise<void> {
    
    this.exercises = await listExercise()
  }

  render(){

    return html`
    <page-template>
    <h1>View Exercice</h1>
    <select-exercise .exercises=${this.exercises}></select-exercise>
    </page-template>
    `
  }




  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-view-exercice': PageViewExercice
  }
}