import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'

@customElement('page-exercice-list')
export class PageExerciceList extends LitElement {

  @property({ type: Object})
  state = {}

  render(){
    return html`
    <page-template>
    <h1>Exercice List</h1>
    </page-template>
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'page-exercice-list': PageExerciceList
  }
}