import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'


@customElement('x-y')
export class XY extends LitElement {

  @property({ type: Object})
  state = {}

  render(){


    return html`
    template
    `
  }

  static get styles(){

    return css`
      :host {
        margin: 0;
      }
    `
  }


}

declare global {
  interface HTMLElementTagNameMap {
    'x-y': XY
  }
}