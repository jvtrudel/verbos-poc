# Fonctionalités à démontrer

## Langues

  - espagnol
  - français

  - [ ] définit/change la langue maternelle
  - [ ] définit/change la langue d'apprentissage
  - [ ] définit/change la langue de travail (application) 

## Tables de conjugaison

  - [x] On peut générer la conjugaison de tout les verbes réguliers 
  - [x] On peut générer des tableaux de conjugaison
  - [x] On peut paramétriser le format des tableaux
  

### 'État' d'un verbe

  - [x] nombre / personne / genre

### Style 

  - [x] couleur de racine/terminaison

## Interactivité

  - [ ] action lorsque clique sur verbe
  - [ ] action lorsque clique sur racine/terminaison

## Réflexivité

  - [ ] On peut déterminer si un mot est un verbe et quelles est son état (temps, nombre, personne, genre).
  - [ ] Reconnait la termisaison et la racine d'un verbe à l'infinitif

## Listes de verbes

  - [ ] CRUD
  - [ ] Persistance
  - [ ] indexation

## Formes du verbe

  - [ ] régulier
  - [ ] irrégularités
  - [ ] mode indicatif
  - [ ] mode subjunctif
  - [ ] mode impératif
  - [ ] mode non personnels
  - [ ] tables de verbes

## Outil de gestion de mots

### dictionnaire: tout les mots

  - [ ] recheche des mots à partir le la distance de levenstein
  - [ ] ajoute un nouveau nom
  - [ ] ajoute un nouveau verbe
  - [ ] ajoute un nouveau ...
  - [ ] ajoute une traduction
  - [ ] ajoute un exemple
  - [ ] ajoute une locution

### lexique: sous-groupe de mots du dictionnaire

  - [ ] ajoute un mot dans un lexique à partir des mots du dictionnaire
  - [ ] retourne la liste des graphies à partir du lexique
  - [ ] affiche tout les mots d'un lexique
  - [ ] dans une phrase, identifier quels mots font partie du lexique

### Outils 





# Références

## Mise en forme de formulaires

  - [HTML Tutorial - Styling a form with CSS](https://www.youtube.com/watch?v=8yrTnjo0TWw)